#!/bin/bash -e

URL=`./finder.sh`

if [ "x${URL}" != "x" ]; then
    echo URL: "${URL}"
    echo "${URL}" > current-url
fi

# Update to handle distributed builds
if cmp current-url old-url; then
    # Don't build
    echo "URL unchanged"
else
    # Update state
    cp current-url old-url
    
    # Trigger the build
    echo RUN_BUILDER
fi

exit 0

