#!/bin/bash -ex

# CONFIG
prefix="Adium"
suffix=""
munki_package_name="Adium"
display_name="Adium"
category="Social Networking"
url=`./finder.sh`

# download it (-L: follow redirects)
curl -L -o app.dmg "${url}"

#mount DMG
mountpoint=`hdiutil attach -mountrandom /tmp -nobrowse app.dmg | awk '/private\/tmp/ { print $3 } '`

#create build-root and copy app
mkdir -p build-root/Applications
app_in_dmg=$(ls -d $mountpoint/*.app)
cp -R $app_in_dmg build-root/Applications

#obtain version info from app
version=`/usr/libexec/PlistBuddy -c "Print :CFBundleVersion" "$app_in_dmg"/Contents/Info.plist`

#build package
/usr/bin/pkgbuild --root build-root/ --identifier edu.umich.izzy.pkg.$munki_package_name --install-location / --version $version --scripts scripts app.pkg

#determine key files for installs array
key_files=`find build-root -name '*.app' -maxdepth 3 | sed 's/ /\\\\ /g; s/^/-f /' | paste -s -d ' ' -`

#generate munki plist
echo /usr/local/munki/makepkginfo app.pkg ${key_files} | /bin/bash > app.plist

## Fixup and remove "build-root" from file paths in plist
perl -p -i -e 's/build-root//' app.plist

# Build pkginfo
#/usr/local/munki/makepkginfo --postinstall_script="postinstall.sh" app.dmg > app.plist

plist=`pwd`/app.plist

# Obtain version info
version=`defaults read "${plist}" version`

# TODO SET THIS and remove the exit -1
# defaults write "${plist}" blocking_applications -array "VirtualBox.app" "/Path/To/Executable" "iTunesHelper"
# defaults write "${plist}" unattended_install -bool YES
# echo 'TODO - configure for unattended install, if it can be'
# exit 42

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/${prefix}-${version}${suffix}.pkg"
defaults write "${plist}" minimum_os_version "10.8.0"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${munki_package_name}"
defaults write "${plist}" display_name "${display_name}"
defaults write "${plist}" category "${category}"
defaults write "${plist}" unattended_install -bool TRUE


# Obtain display name from Izzy and add to plist
display_name=`/usr/local/bin/displaynametool "${munki_package_name}"`
defaults write "${plist}" display_name -string "${display_name}"

# Obtain description from Izzy and add to plist
description=`/usr/local/bin/descriptiontool "${munki_package_name}"`
defaults write "${plist}" description -string "${description}"

# Obtain category from Izzy and add to plist
category=`/usr/local/bin/cattool "${munki_package_name}"`
defaults write "${plist}" category "${category}"


# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"

# Change filenames to suit
mv app.pkg   ${prefix}-${version}${suffix}.pkg
mv app.plist ${prefix}-${version}${suffix}.plist
